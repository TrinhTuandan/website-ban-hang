using Microsoft.EntityFrameworkCore;
using Bai3.Models;
using Microsoft.AspNetCore.Builder;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

var connectionString =
builder.Configuration.GetConnectionString("WebsiteBanHangConnection");
builder.Services.AddDbContext<WebsiteBanHangContext>(options => options.UseSqlServer(connectionString));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllerRoute(
            name: "trang-chu",
            pattern: "trang-chu",
            defaults: new { controller = "Home", action = "Index" });

        endpoints.MapControllerRoute(
            name: "lien-he",
            pattern: "lien-he",
            defaults: new { controller = "Contact", action = "Index" });

        endpoints.MapControllerRoute(
       name: "bai-viet",
       pattern: "bai-viet",
       defaults: new { controller = "Posts", action = "Index" });

        endpoints.MapControllerRoute(
       name: "san-pham",
       pattern: "san-pham",
       defaults: new { controller = "Product", action = "Index" });

        endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");

        endpoints.MapControllerRoute(
        name: "the-loai-san-pham",
        pattern: "{slug}-{id}",
        defaults: new { controller = "Product", action = "CateProd" });

        endpoints.MapControllerRoute(
        name: "chi-tiet-san-pham",
        pattern: "san-pham/{slug}-{id}",
        defaults: new { controller = "Product", action = "ProdDetail" });

        endpoints.MapControllerRoute(
        name: "bai-viet",
        pattern: "bai-viet/{slug}-{id}",
        defaults: new { controller = "Posts", action = "Index" });

        endpoints.MapControllerRoute(
        name: "chi-tiet-bai-viet",
        pattern: "chi-tiet-bai-viet/{slug}-{id}",
        defaults: new { controller = "Posts", action = "ArticleDetails" });

        endpoints.MapControllerRoute(
        name: "chuong-trinh",
        pattern: "chuong-trinh/{slug}",
        defaults: new { controller = "Product", action = "Index" });
    });
app.Run();

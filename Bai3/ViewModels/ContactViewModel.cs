﻿using Bai3.Models;

namespace Bai3.ViewModels
{
    public class ContactViewModel
    {
        public List<Menu> Menus { get; set; }
        public List<Blog> Blogs { get; set; }

    }
}
